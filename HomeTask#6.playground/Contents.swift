import UIKit

/*
    
    Представьте, что вы разрабатываете приложение, как av.by
    Разработайте 2 структуры, с названием Car и CarOwner.
    У каждой из этих структур должны быть свои property и методы (выделите только те, которые важны в данном контексте)
    В теле методов пока просто сделайте вывод в консоль сообщений в зависимости от метода, например
    func goForward() -> {
        print("car is going forward")
    }
    Структура Car должна иметь метод description, который будет выводить в консоль описание автомобиля.
    Стуктура CarOwner "должна знать о своем автомобиле" и должна иметь метод describeCar, при вызове которого в консоль будет выводиться информация о владельце авто и описание самого автомобился.

    Примеры можете взять с av.by
*/

enum Engine {
    case petrol(_ hybrid: String?, engineСapacity: Double)
    case diesel(_ hybrid: String?, engineСapacity: Double)
    case electro(cruisingRange: Int)
    case gas(type: Gas)
    
    enum Gas: String {
        case propane = "propane-butane"
        case methane = "methane"
    }
}

enum Drive: String {
    case front
    case rear
    case connected
    case full
}

enum Transmisson: String {
    case automatic
    case mechanical
}

struct Car {
    var make: String
    var model: String
    var year: Int
    var engine: Engine
    var transmisson: Transmisson
    var body: String
    var color: String
    var mileage: Int
    var drive: Drive
    var price: Int
    
    func description(car: Car) -> Car {
        return car
    }
}

struct CarOwner {
    var name: String
    var numberOfPhone: String
    var location: String
    var car: Car
    func describeCar(_ owner: CarOwner) {
        print("\(name), \(numberOfPhone), \(location), \(car.description(car: car))")
    }
}

let car = Car(make: "Kia",
              model: "Ceed",
              year: 2016,
              engine: .petrol(nil, engineСapacity: 1.6),
              transmisson: Transmisson.mechanical,
              body: "universal",
              color: "gray",
              mileage: 100000,
              drive: Drive.front,
              price: 20000)

let owner = CarOwner(name: "Artem",
                     numberOfPhone: "+375295422570",
                     location: "Minsk",
                     car: car)
owner.describeCar(owner)


