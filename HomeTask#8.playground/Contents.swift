import UIKit

/* 1) Создайте протокол Собеседование. У него есть свойство: фамилия. И методы: successOfinterview и failureOfInterview.
 2) Создайте класс Программист. У него задайте свойства: skill типа String и inSearchOfJob типа Bool. Сделайте их опциональными. Программисту подключите протокол Собеседование.
 3) Добавьте 2 массива, в которые будете добавлять программистов. Вроде okArray и failArray.
 4) В методе successOfinterview сделайте проверку свойства inSearchOfJob. Если true — выводите сообщение «Подходит» и добавляйте имя в массив + сортируйте его по алфавиту.
 5) В методе failureOfInterview просто выводите сообщение: «Не подходит».
 6) Добавьте протокол Техническое собеседование (наследник Собеседования). Добавьте ему свойство isSuitable типа Bool и методы: successTechnicalOfinterview и failureTechnicalOfInterview.
 7) successTechnicalOfinterview проверяет inSearchOfJob и skill: если inSearchOfJob == true, а skill == «ios», то добавляет этого программиста в массив okArray. И выводит сообщение об успешно пройденном собеседовании. «Вы нам подходите. Ждите оффер».
 8) failureTechnicalOfInterview просто выводит сообщение «Нам жаль, но программист(имя) не прошёл собеседование».
 */

protocol Interview {
  var lastName: String { get }
  func successOfInterview()
  func failureOfInterview()
}

protocol TechnicalOfInterview: Interview  {
  var isSuitable: Bool { get }
  func successTechnicalOfInterview()
  func failureTechnicalOfInterview()
}

class Programmer: TechnicalOfInterview {
  var skills: String?
  var inSearchOfJob: Bool?
  var lastName: String
  var isSuitable: Bool {
    skills == "IOS" && inSearchOfJob == true
  }
  
  init(lastName: String) {
    self.lastName = lastName
  }
  
  func successOfInterview() {
    guard inSearchOfJob == true else {
      failureOfInterview()
      return
    }
    print("\(lastName) fits")
  }
  
  func failureOfInterview() {
    guard inSearchOfJob == false else { return }
    print("\(lastName) doesn't fit for the company")
  }
  
  func successTechnicalOfInterview() {
    guard isSuitable else {
      failureTechnicalOfInterview()
      return
    }
    company.addLastNameInSuccessList(lastName)
    print("\(lastName)! You have successfully completed the technical interview. Wait the offer")
  }
  
  func failureTechnicalOfInterview() {
    if !isSuitable {
      print("Unfortunatelly, \(lastName) was failed the technical intreview")
    }
  }
}

//9) Создайте класс Тестировщик. Подключите ему протоколы Собеседование, Техническое собеседование. Также добавьте свойство exp(опыт) типа Int и имя типа String.
//10) Сделайте простую проверку: если опыт больше 3 лет — вызвать метод successOfinterview, который распечатает в консоль: «Тестировщик Егор допущен к техническому собеседованию». А если меньше 3 лет - в методе failureOfInterview просто выводите сообщение: «Егор не подходит».
//11) Добавьте тестировщику свойство skills и 2 массива, по аналогии с Программистом. Метод successTechnicalOfinterview проверяет skills: если skill == «QA», то добавляет этого тестировщика в массив okArray. И выводит сообщение об успешно пройденном собеседовании. «Вы нам подходите. Ждите оффер».
//12) failureTechnicalOfInterview просто выводит сообщение «Нам жаль, но тестировщик (имя) не прошёл собеседование».

class QA: TechnicalOfInterview {
  var isSuitable: Bool {
    skills == "QA" && experience >= 3
  }
  var lastName: String
  var experience: Int
  var skills: String
  
  init(lastName: String, experience: Int, skills: String) {
    self.lastName = lastName
    self.experience = experience
    self.skills = skills
  }
  
  func successOfInterview() {
    if experience >= 3 {
      print("\(lastName) fits")
    }
  }
  
  func failureOfInterview() {
    if experience < 3 {
      print("\(lastName) does't fits")
    }
  }
  
  func successTechnicalOfInterview() {
    guard isSuitable else {
      failureTechnicalOfInterview()
      return
    }
    company.addLastNameInSuccessList(lastName)
    print("\(lastName)! You have successfully completed the technical interview. Wait the offer")
  }
  
  func failureTechnicalOfInterview() {
    if !isSuitable {
      print("Unfortunatelly, \(lastName) was failed the technical intreview")
    }
  }
}

class Company {
  var successInterview: [String] = []
  var failInterview: [String] = []
  
  func addLastNameInSuccessList(_ lastName: String) {
    successInterview.append(lastName)
    successInterview.sort()
  }
  
  func addLastNameInFailList(_ lastName: String) {
    failInterview.append(lastName)
    failInterview.sort()
  }
}

//13) Создайте 3 тестировщиков и программистов.

var company = Company()
let programmer1 = Programmer(lastName: "Ivanov")
//programmer1.inSearchOfJob = true
programmer1.skills = "IOS"
programmer1.successOfInterview()
programmer1.successTechnicalOfInterview()
let programmer2 = Programmer(lastName: "Sidorov")
programmer2.skills = "IOS"
programmer2.inSearchOfJob = true
programmer2.successOfInterview()
programmer2.successTechnicalOfInterview()
let programmer3 = Programmer(lastName: "Petrov")
programmer3.inSearchOfJob = true
programmer3.skills = "Java"
programmer3.successOfInterview()
programmer3.successTechnicalOfInterview()
company.successInterview
let tester1 = QA(lastName: "Baranov", experience: 3, skills: "QA")
let tester2 = QA(lastName: "Kozlov", experience: 3, skills: "QA")
let tester3 = QA(lastName: "Petuxov", experience: 5, skills: "QA")
tester1.successOfInterview()
tester1.successTechnicalOfInterview()
tester1.isSuitable
tester2.successOfInterview()
tester2.successTechnicalOfInterview()
tester3.successTechnicalOfInterview()
company.successInterview


//14) Создайте отдельный тип База Резюме. В нём объедините всех программистов и тестировщиков в общую коллекцию.
//15) В массив failArray добавьте тех, кто не прошёл собеседование. Распределите их по отдельным массивам и найдите в каждом самое длинное имя.
//Выведите его в консоль.

class BaseCV {
  var applicants: [TechnicalOfInterview] = []
}

var baseCV = BaseCV()
baseCV.applicants = [tester1, tester2, tester3, programmer1, programmer2, programmer3]
baseCV.applicants.forEach { applicant in
  if applicant.isSuitable == false {
    company.addLastNameInFailList(applicant.lastName)
  }
}

let longestFailName = company.failInterview.max { $0.count < $1.count }
print(longestFailName ?? "")

let longestSuccessName = company.successInterview.max { $0.count < $1.count }
print(longestSuccessName ?? "")







