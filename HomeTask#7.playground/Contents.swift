import UIKit

/*
 Классы и структуры
 */

/*
 1. Написать простые классы с наследованием и без.
 
 */

class Human {
  var firstName: String
  var lastName: String
  var middleName: String?
  var fullName: String {
    firstName + " " + lastName
  }
  
  init(lastName: String, firstName: String, middleName: String?) {
    self.lastName = lastName
    self.firstName = firstName
    self.middleName = middleName
  }
}

class Student : Human {
  let university: String
  let faculty: String
  var group: Int
  let recordBook: Int
  
  init(firstName: String, lastName: String, university: String, faculty: String, group: Int, recordBook: Int) {
    self.university = university
    self.faculty = faculty
    self.group = group
    self.recordBook = recordBook
    super.init(lastName: lastName, firstName: firstName, middleName: nil)
  }
}

class Animal {
  var name: String
  var age: Int
  
  init(name: String, age: Int ) {
    self.name = name
    self.age = age
  }
}

class Cat: Animal {
  var owner: Human
  
  init(owner: Human, name: String, age: Int) {
    self.owner = owner
    super.init(name: name, age: age)
  }
}

/*
 2. Создать класс *House* и в нем несколько свойств - *width*, *height* и несколько методов - *build* (выводит на экран умножение ширины и высоты), *getWidth* и *getHeight* выводят на экран соответсвенно ширину и высоту.
 */

class House {
  var width: Double
  var height: Double
  
  init(width: Double, height: Double) {
    self.width = width
    self.height = height
  }
  
  func build() -> Double {
    width * height
  }
  
  func getWidth() -> Double {
    width
  }
  
  func getHeight() -> Double {
    height
  }
}

var house = House(width: 12, height: 10)
house.build()
house.getHeight()
house.getWidth()

//    3. Написать класс , а в нем метод который будет принимать букву (одну, может быть и типа string, просто будете передавать ему одну букву) и возвращать все имена которые начинаются на эту букву.
//    К примеру, А - Алексей, Александр, Артем, Антон и т. д. Внутри метода или полем класса (тут как удобно, не сильно критично) будет задаваться массив строк (string) в котором будут прописаны имена. Имена откуда-то подгружать не надо, их надо просто захардкодить. Метод должен возвращать отфильтрованный массив с именами.
//    Так же написать метод, который будет принимать массив строк как аргумент и выводить их всех на консоль с новой строки каждое имя. Им распечатаете на консоль то что у вас получилось отфильтровать.

class Filter {
  func getFilteredArray(letter: String, array: [String]) -> [String] {
    array.filter {$0.contains(letter)
    }
  }
  
  func printArray(_ array: [String]) {
    array.forEach {value in
      print("\(value)\n")
    }
  }
}

var names = ["Artem", "Bob" ,"Alex", "Bill", "Anton", "Andrei", "Mike", "Vasiy", "Sergei"]
var filter = Filter()
filter.getFilteredArray(letter: "A", array: names)

  
/*
 
 4) Написать класс, который формирует массив учеников, сортирует и считает количество этих учеников. Если учеников больше чем 30, выводится сообщение типа «в школе нет мест».
 */

enum Assessment: Int {
  case one = 1
  case two
  case three
  case four
  case five
  case six
  case seven
  case eight
  case nine
  case ten
}

class Pupil: Human {
  var assessments: [Assessment]
  
  init(lastName: String, firstName: String, middleName: String, assessments: [Assessment]) {
    self.assessments = assessments
    super.init(lastName: lastName, firstName: firstName, middleName: middleName)
  }
}

typealias Title = (Int, String)

class Teacher: Human {
  func sortPupils(_ pupils: [Pupil]) -> [Pupil] {
    pupils.sorted {$0.lastName > $1.lastName}
  }
  
  func getCountPupils(_ pupils: [Pupil]) -> Int {
    pupils.count
  }
  
  func gaveAssesment(pupil: Pupil, assesment: Assessment) -> Pupil {
    pupil.assessments.append(Assessment.init(rawValue: assesment.rawValue) ?? .five)
    return pupil
  }
}

class Grade {
  let title: Title
  let teacher: Teacher
  var pupils: [Pupil]
  
  init?( title: Title, teacher: Teacher, pupils: [Pupil]) {
    guard pupils.count <= 30 else { return nil }
    self.teacher = teacher
    self.title = title
    self.pupils = pupils
  }
}

let teacher = Teacher(lastName: "Sidorova", firstName: "Maria", middleName: "Petrovna")
let grade = Grade(title: (1, "A"), teacher: teacher, pupils: [])
var pupil1 = Pupil(lastName: "Ivanov", firstName: "Vasia", middleName: "Petrovich", assessments: [])
var pupil2 = Pupil(lastName: "Sidorov", firstName: "Vova", middleName: "Sergeevich", assessments: [])
grade?.pupils.append(pupil1)
grade?.pupils.append(pupil2)
grade?.teacher.getCountPupils(grade?.pupils ?? [])
grade?.teacher.gaveAssesment(pupil: pupil1, assesment: .ten)



/*
 5) Создать 5 своих структур.
 */

struct Vehicle {
  var countOfWheel: Int
  var color: String
  mutating func paintVehicle(color: String) {
    self.color = color
  }
}

struct Car {
  var engine: String
  let vin: String
  var capacity: Double
  var mileage = 23000
  
  mutating func carMove(kilometre: Double) -> Int {
    self.mileage += Int(kilometre)
    return self.mileage
  }
}

struct Gog {
  var name: String
  var owner: Human
}

struct Competition {
  var location: String
  var data: String
  var title: String
  var place: Int
  var result: String?
}

struct Sportsman {
  let name: String
  var kindOfSport: String
  
  func addAchievements(result: Competition) -> [Competition] {
    var achievement: [Competition] = []
    achievement.append(result)
    return achievement
  }
}


/*
 6) Сделайте список покупок! Программа записывает продукты в массив. Если вызываем определённый продукт — в консоли пишем к примеру «Мёд — куплено!».
 */

struct Food: Equatable {
  var name: String
  var cost: Double
}

struct ListFood {
  private var list: [Food] = []
  
  mutating func addFood(_ food: Food) -> [Food] {
    if checkFood(food) == false {
      self.list.append(food)
      print("\(food.name) was bought")
    }
    return self.list
  }
  
  func checkFood(_ food: Food) -> Bool {
    list.contains(food)
  }
}

var milk = Food(name: "milk", cost: 2)
var bread = Food(name: "bread", cost: 1.5)
var agge = Food(name: "agge", cost: 3)
var list = ListFood()
list.addFood(milk)
list.addFood(bread)
list.addFood(agge)



