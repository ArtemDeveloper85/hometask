import UIKit

/*
    1. Write a function named printFullName that takes two strings called firstName and lastName.
 The function should print out the full name defined as firstName + " " + lastName.
 Use it to print out your own full name.
*/

func printFullName(firstName: String, lastName: String) {
    print(firstName + " " + lastName)
}

printFullName(firstName: "Artem", lastName: "Novikov")

/*
    2. Создайте функцию, которая принимает параметры и вычисляет площадь круга.
*/

func computeSquareCircle(_ radius: Double) -> Double {
    Double.pi * (radius * radius)
}

/*
    3. Создайте функцию, которая принимает параметры и вычисляет расстояние между двумя точками.
*/
typealias Point = (x: Double, y: Double)

func computeDistance(between pointA: Point, and pointB: Point) -> Double {
    let sum = pow((pointB.x - pointA.x), 2) + pow((pointB.y - pointA.y), 2)
    return sum.squareRoot()
}

computeDistance(between: (3, 4), and: (7, 8))


/*
    4. Напишите функцию, которое считает факториал числа.
*/

func computeFactorial(_ number: Int) -> Int {
    var result = 1
    for i in 1...number {
      result *= i
    }
    return result
}

let number = computeFactorial(5)


/*
    5. Напишите функцию, которая вычисляет N-ое число Фибоначчи
*/

func computeFibonacci(n: Int) -> [Int] {
    var sequanceFibonacci: [Int] = []
    for number in 0...n {
        if number == 0 || number == 1 {
            sequanceFibonacci.append(number)
        } else {
            sequanceFibonacci.append(sequanceFibonacci[number - 1] + sequanceFibonacci[number - 2])
        }
    }
   return sequanceFibonacci
}

computeFibonacci(n: 6)

/*
    6. First, write the following function:

 func isNumberDivisible(_ number: Int, by divisor: Int) -> Bool

 You’ll use this to determine if one number is divisible by another. It should return true when number is divisible by divisor.
 
 Hint: You can use the modulo (%) operator to help you out here.
 
 Next, write the main function:

 func isPrime(_ number: Int) -> Bool

 This should return true if number is prime, and false otherwise. A number is prime if it’s only divisible by 1 and itself.
 
 
 You should loop through the numbers from 1 to the number and find the number’s divisors.
 
 
 If it has any divisors other than 1 and itself, then the number isn’t prime. You’ll need to use the isNumberDivisible(_:by:) function you wrote earlier.
 
 Use this function to check the following cases:

    isPrime(6) // false
    isPrime(13) // true
    isPrime(8893) // true

 Hint 1: Numbers less than 0 should not be considered prime. Check for this case at the start of the function and return early if the number is less than 0.
 Hint 2: Use a for loop to find divisors. If you start at 2 and end before the number itself, then as soon as you find a divisor, you can return false.
*/

func isNumberDivisible(_ number: Int, by divisor: Int) -> Bool {
    divisor > 0 && number % divisor == 0
}

func isPrime(_ number: Int) -> Bool {
    guard number > 1 else { return false }
    for i in 2..<number {
        if isNumberDivisible(number, by: i) {
            return false
        }
    }
    return true
}

isPrime(6)
isPrime(13)
isPrime(8893)





