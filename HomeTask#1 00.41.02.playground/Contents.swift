import UIKit

// 1) Напишите переменные и константы всех базовых типов данных:_ int, UInt, float, double, string. У чисел вывести их минимальные и максимальные значения.

var valueInt = 12
let numberInt = 15
Int.max
Int.min

var valueUInt8: UInt8 = 123
let numberUInt8: UInt8 = 100
UInt8.max
UInt8.min

var valueUInt16: UInt16 = 11111
let numberUInt16: UInt16 = 22222
UInt16.max
UInt16.min

var valueUInt32: UInt32 = 1234433335
let numberUInt32: UInt32 = 1005453233
UInt32.max
UInt32.min

var valueUInt64: UInt64 = 12344333354535434343
let numberUInt64: UInt64 = 10054532333553455553
UInt64.max
UInt64.min

var valueInt8: Int8 = 123
let numberInt8: Int8 = 123
Int8.max
Int8.min

var valueInt16: Int16 = 12345
let numberInt16: Int16 = 12345
Int16.max
Int16.min


var valueInt32: Int32 = 1234567899
let numberInt32: Int32 = 1234567890

Int32.max
Int32.min

var valueInt64: Int64 = 1234567899123445556
let numberInt64: Int64 = 1234567890253544757
Int64.max
Int64.min


var valueFloat: Float = 12345678910e+10
let numberFloat: Float = 1.5525746

var valueDouble: Double = 1.676567666575746
let numberDouble: Double = 1.552564547758878

var string = "Hello"
let stringConstant = "Hi"

// 2) Создайте список товаров с различными характеристиками (количество, название). Используйте typealias.

typealias nameTypealias = String
typealias amount = Int


let mackBook: nameTypealias    = "MacBook Air"
let bridge: nameTypealias      = "Samsung"
let dishwasher: nameTypealias  = "Electrolux"
let microvawe: nameTypealias   = "LG"
let player:    nameTypealias   = "Philips"
let mobilePhone: nameTypealias = "IPhone 8"

let mackBookAir: amount = 13
let mackBookPro: amount = 10
let miniMac:     amount = 9
let iMac:        amount = 12

//3) Напишите различные выражения с приведением типа.

let int = 3
let double = 2.6
let say = "Hello"
let number = "63"

let c = int + Int(double)
let d = Double(int) + double
let sayHello = say + String(int)

if let n = Int(number) {
    n + Int(double)
}
// 4) Вычисления с операторами (умножение, деление, сложение, вычитание)
//Результат вычислений должен выводиться в консоль в таком виде: «3 + 2 = 5».

var f = 6
var s = 3
var g = 0
g = f + s
print("\(f) + \(s) = \(g)")
g = f * s
print("\(f) * \(s) = \(g)")
g = f / s
print("\(f) / \(s) = \(g)")
g = f - s
print("\(f) - \(s) = \(g)")

//5) Declare two constants a and b of type Double and assign both a value. Calculate the average of a and b and store the result in a constant named average.
let a = 2.6
let b = 45.5
let average = (a + b ) / 2

//6) A temperature expressed in °C can be converted to °F by multiplying by 1.8 then incrementing by 32. In this challenge, do the reverse: convert a temperature from °F to °C. Declare a constant named fahrenheit of type Double and assign it a value. Calculate the corresponding temperature in °C and store the result in a constant named celcius.

let fahrenheit = 30.0
let celcius = (fahrenheit - 32.0) / 1.8

//7) A circle is made up of 2𝜋 radians, corresponding with 360 degrees. Declare a constant degrees of type Double and assign it an initial value. Calculate the corresponding angle in radians and store the result in a constant named radians.

let pi = Double.pi
let degrees = 240.0

let radians = degrees * pi / Double(180)

//8) Declare four constants named x1, y1, x2 and y2 of type Double. These constants represent the 2-dimensional coordinates of two points. Calculate the distance between these two points and store the result in a constant named distance.
//*/

//AB = √(xb - xa)2 + (yb - ya)2

let x1 = -3.0
let y1 = 3.0
let x2 = -2.0
let y2 = 6.0
let intermediateNumber = ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
let distance = intermediateNumber.squareRoot()



/*
2. Строки

 */
 
//1) Напишите с помощью строк своё резюме: имя, фамилия, возраст, где живете, хобби и т.п. 2) Соберите из этих строк 1 большую (вспоминаем интерполяцию) и выведите в консоль.


let name = "Artem"
let surname = "Novikov"
let age = "35"
let country = "Belarus"
let city = "Minsk"
let email = "artemdeveloper85@gmail.com"
let previousJob = "the Academy of the Ministry of Internal Affairs"
let fitness = "fitness"
let chess = "chess"
let education = "Belarusian State University of Physical Culture"
let link = "https://bitbucket.org/ArtemDeveloper85/workspace/repositories"

let cv = "My name is \(name) \(surname). I'm \(age) years old. I live in \(country). My location is \(city). I graduate \(education) in 2008. My previous working place was \(previousJob). My hobbies are: \(fitness)  and \(chess). This is the link with my projects: \(link). My email is \(email). "




//3) Напишите 10 строк, соберите их с помощью интерполяции и поставьте в нужных местах переносы на новую строку и пробелы (см. \n и \t).

print(cv)

let newCV = "My name is \(name) \(surname).\nI'm \(age) years old.\nI live in \(country). My location is \(city).\nI graduate \(education) in 2008.\nMy previous working place was \(previousJob).\nMy hobbies are: \(fitness)  and \(chess).\nThis is the link with my projects: \t\(link).\nMy email: \t\(email)."

print(newCV)

//4) Разбейте собственное имя на символы, перенося каждую букву на новую строку.

let myName = "A\nr\nt\ne\nm"
print(myName)

//5) Создайте переменную типа Int и переменную типа String.
//Тип Int преобразуйте в String и с помощью бинарного оператора сложите 2 переменные в одну строку.
let digit = 2
let stringDigit = "6"
let newNumber = String(digit) + stringDigit

//6) Create a string constant called firstName and initialize it to your first name. Also create a string constant called lastName and initialize it to your last name.

let firstName = "Artem"
let lastName = "Novikov"

//7) Create a string constant called fullName by adding the firstName and lastName constants together, separated by a space.
let fullName = "\(firstName) \(lastName)"

//8) Using interpolation, create a string constant called myDetails that uses the fullName constant to create a string introducing yourself. For example, my string would read: "Hello, my name is Matt Galloway.".

let myDetails =  "Hello, my name is \(fullName)"





/*
3. Tuples
 
 1) Создать кортеж с 3-5 значениями. Вывести их в консоль 3 способами.
*/

var values = (one: 1, two:2, three: 3, four: 4, five:5)

values

values.0
values.1
values.2
values.3
values.4

values.one
values.two
values.three
values.four
values.five



/*
2) Давайте представим, что мы сотрудник ГАИ и у нас есть какое-то количество нарушителей. Задача. Создать кортеж с тремя параметрами:
- первый - превышение скорости: количество пойманных;
- второй - вождение нетрезвым: количество пойманных;
- третий - бесправники: количество пойманных.
Распечатайте наших бедокуров в консоль через print().
*/


let offenders = (overspeeed: 12, inebriated: 4, withoutDocuments: 3)
print(offenders)

//3) Выведите каждый параметр в консоль. Тремя способами.

offenders

offenders.0
offenders.1
offenders.2
    
offenders.overspeeed
offenders.inebriated
offenders.withoutDocuments

//4) Создайте второй кортеж — нашего напарника. Значения задайте другие.

let offenders2 = (overspeeed: 10, inebriated: 3, withoutDocuments: 1)



//5) Пусть напарники соревнуются: создайте третий кортеж, который содержит в себе разницу между первым и вторым.

let offenders3 = (overspeeed: offenders.overspeeed - offenders2.overspeeed, inebriated: offenders.inebriated - offenders2.inebriated, withoutDocuments: offenders.withoutDocuments - offenders2.withoutDocuments)



//6) Declare a constant tuple that contains three Int values followed by a Double. Use this to represent a date (month, day, year) followed by an average temperature for that date.

var today = ( month: 7, day: 30, year: 2021, temperature: 24.0)



//    7) Change the tuple to name the constituent components. Give them names related to the data that they contain: month, day, year and averageTemperature.

let (month, day, year, averageTemperature) = today


//    8) In one line, read the day and average temperature values into two constants. You’ll need to employ the underscore to ignore the month and year.

let (_, currentday, _, temp) = today
currentday
temp


//    9) Up until now, you’ve only seen constant tuples. But you can create variable tuples, too. Change the tuple you created in the exercises above to a variable by using var instead of let. Now change the average temperature to a new value.
//*/


today.temperature = 30
today
