import UIKit

/*
 Task 1
 
 Create a variable named counter and set it equal to 0.
 Create a while loop with the condition counter < 10 which prints out counter is X (where X is replaced with counter value) and then increments counter by 1.
 
 */

var counter = 0
while counter < 10 {
    
    counter += 1
    print(counter)
}


/*
 Task 2
 
 Create a variable named counter and set it equal to 0. Create another variable named roll and set it equal to 0.
 Create a repeat-while loop. Inside the loop, set roll equal to Int.random(in: 0...5) which means to pick a random number between 0 and 5.
 Then increment counter by 1. Finally, print After X rolls, roll is Y where X is the value of counter and Y is the value of roll.
 Set the loop condition such that the loop finishes when the first 0 is rolled.
 
 */
var counter1 = 0
var roll = 0
repeat {
    roll = Int.random(in: 0...5)
    counter1 += 1
    print("Roll is :\(roll)")
    print("Counter is : \(counter1)")
    
} while roll > 0

/*
 Task 3
 
 Create a constant named range, and set it equal to a range starting at 1 and ending with 10 inclusive.
 Write a for loop that iterates over this range and prints the square of each number.
 */

let range = 1...10
for i in range {
    print(i*i)
}


/*
 Task 4
 
 Below you can see an example of for loop that iterates over only the even rows like so:
 */

var sum = 0
for row in 0..<8 {
    if row % 2 == 0 {
        continue
    }
    
    for column in 0..<8 {
        print("Here is \(row)")
        sum += row * column
        
    }
    print(sum)
}


/*
 Change this to use a where clause on the first for loop to skip even rows instead of using continue. Check that the sum is 448 as in the initial example.
 */

var sum1 = 0
for row in 0..<8 where row % 2 == 1 {
    
    for column in 0..<8 {
        print("Here is \(row)")
        sum1 += row * column
        
    }
    print(sum1)
}



/*
 Task 5
 Print a table of the first 10 powers of 2
 
 */


var number = 2
var result = 1
for _ in 1...10 {
    result *= 2
    print(result)
}



/*
 Task 6
 
 Given a number n, calculate the factorial of n.
 Example: 4 factorial is equal to 1 * 2 * 3 * 4
 */

var n = 7
var total = 1
for i in 1...n {
    total *= i
    print(total)
}


/*
 Task 7
 
 Given a number n, calculate the n-th Fibonacci number. (Recall Fibonacci is 1, 1, 2, 3, 5, 8, 13, ... Start with 1 and 1 and add these values together to get the next value. The next value is the sum of the previous two. So the next value in this case is 8+13 = 21.)
 */

var n1 = 8
var fibonacciArray: [Int] = []
var numberForFibonacci = 0
for index in 0...n1 {
    if index == 0 || index == 1 {
        fibonacciArray.append(index)
    } else {
        fibonacciArray.append(fibonacciArray[index-1] + fibonacciArray[index-2])
    }
}

/*
 Task 8
 
 Write a switch statement that takes an age as an integer and prints out the life stage related to that age. You can make up the life stages, or use my categorization as follows: 0-2 years, Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult; 40-60, Middle aged; 61+, Elderly.
 */
var age = 121

switch age {
    case 0...2:
        print("Infant")
    case 3...12 :
        print("Child")
    case 13...19:
        print("Teenager")
    case 20...39:
        print("Adult")
    case 40...60:
        print("Middle")
    case 61...130:
        print("Elderly")
    default:
        print("People don't live like that long")
}



/*
 Task 9
 
 Write a switch statement that takes a tuple containing a string and an integer. The string is a name, and the integer is an age. Use the same cases that you used in the previous exercise and let syntax to print out the name followed by the life stage. For example, for myself it would print out "Slava is an adult."
 */

let person = (name: "Artem", age: 35)

switch person.age {
    case 0...2:
        print("\(person.name) is Infant")
    case 3...12 :
        print("\(person.name) is Child")
    case 13...19:
        print("\(person.name) is Teenager")
    case 20...39:
        print("\(person.name) is Adult")
    case 40...60:
        print("\(person.name) is Middle")
    case 61...130:
        print("\(person.name)is  Elderly")
    default:
        print("People don't live like that long")
}


/*
 Task 10
 
 Create a constant called myAge and set it to your age. Then, create a constant named isTeenager that uses Boolean logic to determine if the age denotes someone in the age range of 13 to 19.
 */

let myAge = 19
let isTeenager = myAge >= 13 && myAge <= 19



/*
 Task 11
 Create another constant named theirAge and set it to my age, which is 30. Then, create a constant named bothTeenagers that uses Boolean logic to determine if both you and I are teenagers.
 */

let theirAge = 30
let bothTeenagers = theirAge >= 13 && theirAge <= 19 && myAge >= 13 && myAge <= 19


/*
 Task 12
 Create a constant named reader and set it to your name as a string. Create a constant named author and set it to my name, Matt Galloway. Create a constant named authorIsReader that uses string equality to determine if reader and author are equal.
 */


let reader = "Artem"
let author = "Matt Galloway"
let authorIsReader = reader == author

/*
 Task 13
 
 Создайте массив "дни в месяцах":
 Распечатайте элементы, содержащие количество дней в соответствующем месяце, используя цикл for и этот массив.
 */


let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30 , 31, 30, 31]
var totalDays = 0

for (index, value) in daysInMonths.enumerated() {
    print("\(value) days in month number \(index + 1)  ")
}



/*
 Task 14
 Создать в if и отдельно в switch программу которая будет смотреть на возраст человека и говорить куда ему идти в школу, в садик, в универ, на работу или на пенсию и тд.
 */

let personAge = 47

if personAge == 0 {
    print("You weren't born yet")
} else if personAge >= 1 && personAge <= 2 {
    print("You need to sleep")
} else if personAge >= 3 && personAge <= 6 {
    print("You need to go to kindergarden")
} else if personAge >= 7 && personAge <= 17 {
    print("You need to go to school")
} else if personAge >= 18 && personAge <= 23 {
    print("You need to go to university")
} else if  personAge >= 24 && personAge <= 60 {
    print("You need to go to work")
} else {
    print("You need to relax because you are  pensioner")
}

switch  personAge  {
    case 0:
        print("You weren't born yet")
    case 1...2:
        print("You need to sleep")
    case 3...6:
        print("You need to go to kindergarden")
    case 7...17:
        print("You need to go to school")
    case 18...23:
        print("You need to go to university")
    case 24...60:
        print("You need to go to work")
    default:
        print("You need to relax because you are  pensioner")
}



/*
 Task 15
 В switch и отдельно в if создать систему оценивания школьников по 12 бальной системе и высказывать через print мнение.
 */

let assessment = 6
switch  assessment  {
    case 0:
        print("You know nothing\u{1F621}")
    case 1:
        print("You know nothing almost\u{1F615}")
    case 2:
        print("You know little more than nothing\u{1F617}")
    case 3:
        print("This is negative assessment too\u{1F624}")
    case 4:
        print("You need to learn more \u{1F612}")
    case 5:
        print("This is average assessment almost\u{1F620}")
    case 6:
        print("This is average assessment \u{1F611}")
    case 7:
        print("If you want to get better assessment you need to be more diligent\u{1F605}")
    case 8:
        print("Good assessment but there’s no limit to perfection\u{1F603}")
    case 9:
        print("Nice assessment but there’s no limit to perfection\u{1F603}")
    case 10:
        print("Excellently\u{1F600}")
    case 11:
        print("Perfectly\u{1F610}")
    case 12:
        print("You are boy wonder\u{1F603}")
    default:
        print("No such assessment")
}

if assessment == 0 {
    print("You know nothing\u{1F621}")
} else if assessment == 1 {
    print("You know nothing almost\u{1F615}")
} else if assessment == 2 {
    print("You know little more than nothing\u{1F617}")
} else if assessment == 3 {
    print("This is negative assessment too\u{1F624}")
} else if assessment == 4 {
    print("You need to learn more \u{1F612}")
} else if assessment == 5 {
    print("This is average assessment almost\u{1F620}")
} else if assessment == 6 {
    print("This is average assessment \u{1F611}")
} else if assessment == 7 {
    print("If you want to get better assessment you need to be more diligent\u{1F605}")
} else if assessment == 8 {
    print("Good assessment but there’s no limit to perfection\u{1F603}")
} else if assessment == 9 {
    print("Nice assessment but there’s no limit to perfection\u{1F603}")
} else if assessment == 10 {
    print("Excellently\u{1F600}")
} else if assessment == 11 {
    print("Perfectly\u{1F610}")
} else if assessment == 12 {
    print("You are boy wonder\u{1F603}")
} else {
    print("No such assessment")
}











