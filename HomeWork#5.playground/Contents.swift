import UIKit

/*
 1.
 Define a function with input parameter: [String?] (array). Function should return an array of type [String]
 For example:
 input array: ["a", nil, "b"]
 output array: ["a", "b"]
 */

func getArrayWithoutOptional(_ array: [String?]) -> [String] {
    var stringArray = [String]()
    for string in array {
        if let string = string {
            stringArray.append(string)
        }
    }
    return stringArray
}

let str = getArrayWithoutOptional(["a", nil, "b"])



/*:
 2.
 - Объявите функцию checkMinMax, которая принимает 2 именованых аргумента типа Double min и max и возвращает true если min < max
 */


func checkMinMax(min: Double, max: Double) -> Bool {
    min < max
}

/*
 3.
 - Объявите функцию meanValue, которая принимает 2 неименованых аргумента типа Double и возвращает их среднее значение
 */


func meanValue(_ a: Double, _ b: Double) -> Double {
    (a + b) / 2
}


/*
 4.
 - Объявите функцию meanValue, которая принимает 1 аргумент типа [Int] и возвращает среднее значение всех элементов массива
 */

func meanValue(_ array: [Int]) -> Double {
    var sum = 0
    for number in array {
        sum += number
    }
    return Double(sum) / Double(array.count)
}


/*
 5.
 - Объявите функцию validPerson, которая принимает на вход имя, фамилию, возраст человека и возвращает строку "FirstName LastName, возраст N лет"
 
 - При этом имя и фамилия должны быть длинее 1 символа, а возраст от 0 до 200 лет. Иначе выводится nil
 
 - Напечатайте в консоль результат работы функции для. Ю Сянь, которому 20 лет и Иванова Сергея, которому 34 года
 */

func validPerson(name: String, surname: String, age: Int) -> String? {
    guard name.count > 1,
          surname.count > 1,
          age >= 0,
          age <= 200 else {
        return nil
    }
    return name + " " + surname + " " + "\(age)"
}

print(validPerson(name: "Сергей", surname: "Иванов", age: 34) ?? "")
print(validPerson(name: "Ю", surname: "Сянь", age: 20) ?? "")



/*
 6. Сделайте сортировку пузырьком (bubble sort).
 */


func bubbleSort(array: inout [Int]) -> [Int] {
    for i in 0..<array.count {
        for j in 1..<array.count - i {
            if array[j] < array[j-1] {
                let tmp = array[j-1]
                array[j-1] = array[j]
                array[j] = tmp
            }
        }
    }
    return array
}
var array1 = [6,7,1,4,7,9]
bubbleSort(array: &array1)


/*
 7.
 Создайте словарь, где ключ — фамилия солдата, а значение — его приветствие.
 В цикле пройдитесь по всем ключам и распечатайте фамилию каждого солдата.
 Сделайте тоже самое со значениями и распечатайте приветствие каждого солдата.
 Создайте логическую проверку: если ключ словаря — Иванов, то скажите, что это снайпер. Сделайте тоже самое со всеми ключами.
 */

var dictionary = [
    "Petrov": "Yes, that's right, sir",
    "Ivanov": "Yes sir, no sir, right away sir",
    "Sidorov": "Sir, let me go"
]

for keys in dictionary.keys {
    print(keys)
}
for value in dictionary.values {
    print(value)
}

for key in dictionary.keys {
    if key == "Ivanov" {
        print("sniper")
    } else if key == "Sidorov" {
        print("pilot")
    } else if key == "Petrov" {
        print("foot soldier")
    }
}



//1) Напишите как понимаете enumerations: что это такое, в чем их смысл, зачем нужны. Ваше личное мнение: как и где их можно использовать?
/*
 
 Enumerations - тип данных, который позволяет хранить предопределенные значения. Используются для хранения альтернативных значений (при использовании связанных значений(RawValue) не позволяет хранить их одинаковыми).
 
 */

//2) Написать по 5 enum разных типов + создать как можно больше своих enumerations. Главное, соблюдайте правила написания: понятность и заглавная буква в начале названия. Пропустите их через switch и выведите в консоль.

enum SolarSystem: Int {
    case mercury = 1, venus, earth, mars, jupiter, saturn, uran, neptun
}

var planets: SolarSystem = .uran

switch planets {
    case .mercury:
        print("Mercury is \(planets.rawValue)-st planet from the sun")
    case .venus:
        print("Venus is \(planets.rawValue)-nd planet from the sun")
    case .earth:
        print("Earth is \(planets.rawValue)-rd planet from the sun")
    case .mars:
        print("Mars is \(planets.rawValue)- th planet from the sun")
    case .jupiter:
        print("Jupiter is \(planets.rawValue)-th planet from the sun")
    case .saturn:
        print("Saturn is \(planets.rawValue)-th planet from the sun")
    case .uran:
        print("Uran is \(planets.rawValue)-th planet from the sun")
    case .neptun:
        print("Neptun is \(planets.rawValue)-th planet from the sun")
        
}

enum ProductsOfApple {
    case macBook(model: String, serialNumber: String)
    case iMac(model: String, serialNumber: String)
    case miniMac(model: String, serialNumber: String)
    case iPhone(model: String, serialNumber: String)
}

var macBook: ProductsOfApple = .macBook(model: "MacBook Air(M1)", serialNumber: "FVFFJ1T2Q6LR")

switch macBook {
    case .macBook(model: let model, serialNumber: let serialNumber):
        print("\(model) \(serialNumber)")
    default:
        print("No devices")
}

typealias Day = Int

enum Month {
    case january(countOfDays: Day)
    case february(countOfDays: Day)
    case marth(countOfDays: Day)
    case april(countOfDays: Day)
    case may(countOfDays: Day)
    case june(countOfDays: Day)
    case july(countOfDays: Day)
    case august(countOfDays: Day)
    case september(countOfDays: Day)
    case october(countOfDays: Day)
    case november(countOfDays: Day)
    case december(countOfDays: Day)
    
    static var allCases: [Month] = [.january(countOfDays: 31),
                                    .february(countOfDays: 28),
                                    .marth(countOfDays: 31),
                                    .april(countOfDays: 30),
                                    .may(countOfDays: 31),
                                    .june(countOfDays: 30),
                                    .july(countOfDays: 31),
                                    .august(countOfDays: 31),
                                    .september(countOfDays: 30),
                                    .october(countOfDays: 31),
                                    .november(countOfDays: 30),
                                    .december(countOfDays: 31)]
}

Month.allCases.forEach {print ($0)}

enum Corrency {
    case dollar(country: String, symbol: String)
    case euro(country: [String], symbol: String)
    case ruble(country: String, symbol: String)
    case pound(country: String, symbol: String)
    case yen(country: String, symbol: String)
    case yuan(country: String, symbol: String)
}

let currencies: [Corrency] = [.dollar(country: "USA", symbol: "＄"),
                              .euro(country: ["Germany", "Italy", "England", "Belgium", "France"], symbol: "€"),
                              .ruble(country: "Russia", symbol: "₽"),
                              .pound(country: "Great Britain", symbol: "£"),
                              .yen(country: "Japan", symbol: "¥"),
                              .yuan(country: "China", symbol: "¥")]

for corrency in currencies {
    switch corrency {
        
        case .dollar(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
        case .euro(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
        case .ruble(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
        case .pound(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
        case .yen(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
        case .yuan(country: let country, symbol: let symbol):
            print("\(country) \(symbol)")
            
    }
}

//3) Создайте своё резюме с использованием enum: имя, фамилия, возраст, профессия, навыки, образование, хобби и т.д. - пункты на ваше усмотрение.

enum CV: String {
    case firstName = "Artem"
    case lastName = "Novikov"
    case age = "35"
    case profession = "IOS developer"
    case skills = "Xcode, Swift, Git"
    case education = "TeachMeSkills IOS developer course"
    case hobbies = "chess, fitness, science"
}


//4) Представьте, что вы попали на завод Apple. Вам принесли MacBook, Iphone, Ipad, Apple Watch и сказали: «Раскрась их в разные цвета. Джони Айву нужно вдохновение».
//Вы подвисли и начали раскрашивать. В итоге у вас получился красный MacBook, зеленый Ipad, розовый Iphone и буро-коричневый Apple Watch.
//Инструкция: для цветов задаём через enumeration. Наши девайсы располагаем в теле функции.
//Итог программы: «Айфон — розовый».


enum Color {
    case red
    case green
    case pink
    case brown
    
    enum Device {
        case macBook, ipad, iphone, appleWatch
    }
    
    func drawDevice(_ color: Color) {
        switch color {
            case .red:
                print("\(Color.Device.macBook) - \(self)")
            case .green:
                print("\(Color.Device.ipad) - \(self)")
            case .pink:
                print("\(Color.Device.iphone) - \(self)")
            case .brown:
                print("\(Color.Device.appleWatch) - \(self)")
        }
    }
}

var color: Color = .pink
color.drawDevice(color)


//5) Declare a enumerate of type "Colors" and assign the enumerate members related values of type "String"
//declare a function that takes 2 arguments:
//1-st: of type "String"
//2-nd: of type "Colors" where does it have a default value
//if the name of the string matches the color that is in "Colors", then the string is converted to color, otherwise the default value is returned

enum Colors: String, CaseIterable {
    case red
    case green
    case yellow

    func getColor(name: String, color: Colors = .red) -> Colors {
        Colors.init(rawValue: name) ?? color
    }
}

Colors.green.getColor(name: "black")


//Challenge 1:
//
//You have an enum of coins:
//
//enum Coin: Int {
//  case penny = 1
//  case nickel = 5
//  case dime = 10
//  case quarter = 25
//}
//
//and an array:
//
//let coinPurse: [Coin] = [.penny, .quarter, .nickel, .dime, .penny, .dime, .quarter]
//
//Write a function where you can pass in the array of coins, add up the value and then return the number of cents.
//

enum Coin: Int {
    case penny = 1
    case nickel = 5
    case dime = 10
    case quarter = 25
}

let coinPurse: [Coin] = [.penny, .quarter, .nickel, .dime, .penny, .dime, .quarter]

func countMoney(in coins: [Coin]) -> Int {
    var sum = 0
    for coin in coins {
        sum += coin.rawValue
    }
    return sum
}
countMoney(in: coinPurse)

//Challenge 2:
//
//Here is an example of Direction enumeration:
//
//enum Direction {
//  case north
//  case south
//  case east
//  case west
//}
//
//Imagine starting a new level in a video game. The character makes a series of movements in the game. Calculate the position of the character on a top-down level map after making a set of movements:
//let movements: [Direction] = [.north, .north, .west, .south,
//  .west, .south, .south, .east, .east, .south, .east]
//Hint: Use a tuple for the location:
//var location = (x: 0, y: 0)
//Hint: start position = (x: 0, y: 0)

typealias Point = (x: Int, y: Int)

enum Direction {
    case north(Point)
    case south(Point)
    case east(Point)
    case west(Point)
}

var location: Point = (0, 0)
let movements: [Direction] = [.north((0,7)),
                              .north((0,4)),
                              .west((-4,0)),
                              .south((0,3)),
                              .west((-2,0)),
                              .south((0,-3)),
                              .south((0,-2)),
                              .east((4,0)),
                              .east((2,0)),
                              .south((0,-3)),
                              .east((1,0))]

func calculateLocation(movements: [Direction]) -> (Point) {
    for movement in movements {
        switch movement {
            case .north((let x, let y)):
                location.x += x
                location.y += y
            case .south((let x, let y)):
                location.x += x
                location.y += y
            case .east((let x, let y)):
                location.x += x
                location.y += y
            case .west((let x, let y)):
                location.x += x
                location.y += y
        }
    }
    return location
}

print(calculateLocation(movements: movements))


